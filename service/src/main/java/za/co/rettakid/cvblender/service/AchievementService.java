package za.co.rettakid.cvblender.service;

import za.co.rettakid.cvblender.common.dto.AchievementDto;

/**
 * Created by Lwazi on 6/2/2016.
 */
public interface AchievementService {
    AchievementDto getAchievements(Long id);
}
