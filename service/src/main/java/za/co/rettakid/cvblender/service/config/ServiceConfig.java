package za.co.rettakid.cvblender.service.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by Lwazi on 6/2/2016.
 */
@Configuration
@EnableAutoConfiguration
@ComponentScan("za.co.rettakid.cvblender.service")
public class ServiceConfig {

}
