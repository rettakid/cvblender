package za.co.rettakid.cvblender.common.dto;

import java.util.Date;

/**
 * Created by Lwazi on 6/2/2016.
 */
public class AchievementDto {

    private Long id;
    private String heading;
    private String body;
    private Date validFrom;

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

}
