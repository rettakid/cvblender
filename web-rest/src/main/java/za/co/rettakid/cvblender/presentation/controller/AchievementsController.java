package za.co.rettakid.cvblender.presentation.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import za.co.rettakid.cvblender.common.dto.AchievementDto;
import za.co.rettakid.cvblender.service.AchievementService;

/**
 * Created by Lwazi on 6/2/2016.
 */
@RestController
@RequestMapping("/achievements")
public class AchievementsController {

    private static final Logger logger = LoggerFactory.getLogger(AchievementsController.class);
    private AchievementService achievementService;

    @Autowired
    public AchievementsController(AchievementService achievementService) {
        this.achievementService = achievementService;
    }

    @RequestMapping("/{achievementId}")
    public AchievementDto getAchievement(@PathVariable("achievementId") Long achievementId)  {
        logger.info("getting achievement : {}",achievementId);
        return achievementService.getAchievements(achievementId);
    }

}
