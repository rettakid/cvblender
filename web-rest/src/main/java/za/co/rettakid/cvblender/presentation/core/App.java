package za.co.rettakid.cvblender.presentation.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import za.co.rettakid.cvblender.service.config.ServiceConfig;

/**
 * Created by Lwazi on 6/2/2016.
 */
@SpringBootApplication(scanBasePackages = "za.co.rettakid.cvblender.presentation")
@Import(ServiceConfig.class)
public class App {

    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

}
